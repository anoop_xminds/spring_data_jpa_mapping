package com.test.jpa.ExceptionService;

public class CustomException extends RuntimeException{
    public CustomException(String err){
        super(err);
    }
}
