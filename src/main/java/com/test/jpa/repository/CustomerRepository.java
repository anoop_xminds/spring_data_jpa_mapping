package com.test.jpa.repository;

import com.test.jpa.dto.OrderResponce;
import com.test.jpa.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerRepository
        extends JpaRepository<Customer, Long> {

    @Query("SELECT new com.test.jpa.dto.OrderResponce(c.name, p.p_name) FROM Customer c JOIN c.products p")
    public List<OrderResponce> getJoinInformation();
}
