package com.test.jpa.GlobalHandler;

import com.test.jpa.ExceptionService.CustomErrorResponse;
import com.test.jpa.ExceptionService.CustomException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionService {
    @ExceptionHandler
    ResponseEntity<CustomErrorResponse> handleException(CustomException err){
        CustomErrorResponse CER =new CustomErrorResponse();
        CER.setStatus(HttpStatus.NOT_FOUND.value());
        CER.setMessage("You have entered id :"+ err.getMessage() +" Which is more than 100 or less 0");
        return new ResponseEntity<>(CER,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    ResponseEntity<CustomErrorResponse> handleException(Exception exe){
        CustomErrorResponse CER = new CustomErrorResponse();
        CER.setStatus(HttpStatus.BAD_REQUEST.value());
        CER.setMessage(exe.getMessage());
        return new ResponseEntity<>(CER, HttpStatus.NOT_FOUND);
    }
}

