package com.test.jpa.controller;

import com.test.jpa.ExceptionService.CustomException;
import com.test.jpa.dto.OrderRequest;
import com.test.jpa.dto.OrderResponce;
import com.test.jpa.entity.Customer;
import com.test.jpa.repository.CustomerRepository;
import com.test.jpa.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/placeOrder")
    public Customer placeOrder(@RequestBody OrderRequest request){
        return  customerRepository.save(request.getCustomer());
    }

    @GetMapping("/findAllOrders")
    public List<Customer> findAllOrders(){
        return customerRepository.findAll();
    }

    @GetMapping("/getInfo")
    public List<OrderResponce> getJoinInformation(){
        return customerRepository.getJoinInformation();
    }

    @GetMapping("product/{id}")
    public String getProduct(@PathVariable int id){
        String msg;
        if(id>0 && id< 100){
            msg = "You have entered valid id :"+ id;
        }
        else{
            throw new CustomException(""+ id);
        }
        return msg;
    }
}
