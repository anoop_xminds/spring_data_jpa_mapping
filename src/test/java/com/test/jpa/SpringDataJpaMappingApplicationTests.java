package com.test.jpa;

import com.test.jpa.entity.Customer;
import com.test.jpa.entity.Product;
import com.test.jpa.repository.CustomerRepository;
import com.test.jpa.repository.ProductRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.*;

@SpringBootTest
class SpringDataJpaMappingApplicationTests {

//	@Test
//	void contextLoads() {
//	}
	@Autowired
	ProductRepository productRepository;

	@Test
	public void testCreate(){
		Product product = new Product();
		product.setPid(1);
		product.setP_name("Test Product1");
		product.setQuantity(1);
		product.setPrice(100);

		productRepository.save(product);
		assertNotNull(productRepository.findById(1).get());
	}

	@Test
	public  void testReadProduct(){

		Product product = productRepository.findById(1).get();
		assertEquals(10, 100, product.getPrice());
	}
}
